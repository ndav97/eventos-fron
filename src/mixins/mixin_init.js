import {
  mapState,
  mapActions
} from 'vuex'

export default {
    created () {
      this.init__()
    },
  
    computed: {
      ...mapState({
        identity: state => state.identity.identity,
        token   : state => state.identity.token,
        isLogged: state => state.identity.isLogged,
      }),
    },
  
    methods: {
      ...mapActions({
        getIdentity: 'identity/getIdentity',
        getToken   : 'identity/getToken',
        getIsLogged: 'identity/getIsLogged',
      }),
  
      init__: function () {
        this.getIdentity()
        this.getToken()
        this.getIsLogged()
  
        this.$axios.defaults.headers.common = {
          token: this.token,
        }
      },
    }
  }
  