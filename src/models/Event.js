import api from '../utils/api'

const urlBase = 'v1/events'

export default class Event {
  constructor(
    id = 0,
    nombre = '',
    lugar = '',
    fecha = '',
    user_id= 0,
  ) {
    this.id = id
    this.nombre = nombre
    this.lugar = lugar
    this.fecha = fecha
    this.user_id = user_id
  }

  static fromObj(obj) {
    return new Event(
      obj.id,
      obj.nombre ,
      obj.lugar,
      obj.fecha,
      obj.user_id
    )
  }

  //Obtiene todos los eventos
  static async index() {
    try {
      const {data, status} = await api.get(`${urlBase}`)
      if (status === 200) {
        return data
      }
    } catch (error) {
      return false
    }
  }

  /*
    Obtiene los eventos por el id del usuario
    id: Int
  */
  static async byUserId(id) {
    try {
      const {data, status} = await api.get(`${urlBase}/by-user/${id}`)
      if (status === 200) {
        return data
      }
    } catch (error) {
      return false
    }
  }

  //Crea un evento
  async store() {
    try {
      const {data, status} = await api.post(`${urlBase}`, this)
      if (status === 200) {
        return data
      }
    } catch (error) {
      return false
    }
  }
}
