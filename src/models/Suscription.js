import api from '../utils/api'

const urlBase = 'v1/suscriptions'

export default class Event {
  constructor(
    id = 0,
    user_id= 0,
    event_id= 0,
  ) {
    this.id = id
    this.user_id = user_id
    this.event_id = event_id
  }

  static fromObj(obj) {
    return new Event(
      obj.id,
      obj.user_id ,
      obj.event_id
    )
  }

  /*
    Obtiene las suscripciones de un usuario
    id: Int
    */
  static async byUserId(id) {
    try {
      const {data, status} = await api.get(`${urlBase}/by-user/${id}`)
      if (status === 200) {
        return data
      }
    } catch (error) {
      return false
    }
  }

  //Crea la suscripcióon a un evento
  async store() {
    try {
      const {data, status} = await api.post(`${urlBase}`, this)
      if (status === 200) {
        return data
      }
    } catch (error) {
      return false
    }
  }
}
