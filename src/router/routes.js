const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Login.vue") },
      {
        path: "/inicio",
        name: "Inicio",
        component: () => import("pages/Index.vue")
      },
      {
        path: "/owns",
        name: "Owns",
        component: () => import("pages/OwnEvents.vue")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
