const identity_update = (state, payload) => {
    state.identity = payload
}

const token_update =  (state, payload) => {
    state.token = payload
}

const islogged_update = (state, payload) => {
    state.isLogged = payload
}

export default {
    identity_update,
    token_update,
    islogged_update
}

