export default function () {
  return {
    identity: null,
    token: null,
    isLogged: false,
  }
}
